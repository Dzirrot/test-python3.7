from www.server.task.base_task import BaseTask
from www.server.task.task_manager import TaskManager


@TaskManager.register()
class Reverse(BaseTask):
    _type = 'reverse'
    _lead_time = 3

    def complete_task(self):
        """Разворот строки."""
        self._result = self._text[::-1]


@TaskManager.register()
class SwapPairs(BaseTask):
    _type = 'swap'
    _lead_time = 7

    def complete_task(self):
        """Перестановка чётных букв с нечётными."""
        result = ''
        length_text = len(self._text)
        for i in range(0, length_text - 1, 2):
            result += self._text[i + 1] + self._text[i]
        result += self._text[-1] if length_text % 2 != 0 else ''

        self._result = result
