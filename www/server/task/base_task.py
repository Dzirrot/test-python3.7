import time
from enum import Enum


class TaskState(Enum):
    """Класс состояний задачи."""
    IN_QUEUE = 'В очереди'

    IN_PROGRESS = 'Выполняется'

    DONE = 'Выполнено'


class BaseTask:
    _status = None
    _result = 'Не найдено.'
    _lead_time = None
    _task_execution_time = None

    def __init__(self, text: str) -> None:
        """
        Конструктор

        :param str text: Строка для обработки.
        """
        self._text = text

    @classmethod
    def create_instance(cls, type_: str, text: str):
        """
        Создать экземпляр класса.
        :param str type_: Тип задачи
        :param str text: Строка для обработки
        """
        return cls(text) if type_ == cls._type else None

    def run_task(self):
        """Запустить выполнение задачи."""
        self.wait()
        self.complete_task()
        self.status = TaskState.DONE

    def complete_task(self):
        """Выполнить действие."""
        raise NotImplemented

    def wait(self):
        """Ожидание выполнения."""
        time.sleep(self._lead_time)

    @property
    def status(self) -> str:
        """Получить статус задачи."""
        return self._status

    @status.setter
    def status(self, value) -> None:
        """Указать статус задачи."""
        self._status = value

    @property
    def result(self) -> str:
        """Получить результат задачи."""
        return self._result
