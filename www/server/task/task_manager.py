"""
Менеджер задач
"""
from www.server.task.base_task import BaseTask

__author__ = 'aa.chumakin'


class _MetaManager(type):
    """Метакласс для контроля получения и установки аттрибутов"""

    def __getitem__(cls, key):
        """Получить значение с помощью синтаксиса словаря."""
        return getattr(cls, key)

    def __getattr__(cls, name):
        """Получение настройки по имени."""

        try:
            return cls._docs[name]
        except KeyError:
            raise KeyError('Попытка получить несуществующую команду: {}'.format(name))


class TaskManager(metaclass=_MetaManager):
    """Менеджер команд."""

    # Список доступных документов
    _tasks = {}

    @staticmethod
    def register():
        def decorator(task):

            assert task.__base__ in [BaseTask], (
                'Задача должена быть наследником класса BaseTask'
            )

            assert task.__name__ not in TaskManager._tasks, (
                f'Попытка зарегистрировать одинаковые типы задач {task.__name__}'
            )

            TaskManager._tasks[task.__name__] = task

            return task

        return decorator

    @classmethod
    def get_all_tasks(cls):
        """
        Получить список всех зарегестрированых задач

        :rtype: List[class]
        """

        return [doc for doc in cls._tasks.values()]

    @classmethod
    def parse(cls, method, text):
        """
        Разобрать команду.

        :param str method: Метод обработки
        :rtype BaseTask
        :return: Экземпляр соответствующей задачи
        :exception Exception: Если не нашли подходящий тип задачи
        """
        try:
            for task_type in TaskManager.get_all_tasks():
                task = task_type.create_instance(method, text)
                if task is not None:
                    return task

        except Exception:
            raise Exception(_UNKNOWN_ERROR)

        raise Exception(_ERROR_PARSE)


_ERROR_PARSE = '''
Извини, но я не смог разобрать команду.
Для справки используй `!help`.
'''

_UNKNOWN_ERROR = '''
Не предвиденная ошибка!
'''
