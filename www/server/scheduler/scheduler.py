from queue import Queue
from threading import Thread

from www.server.task.base_task import BaseTask, TaskState


class Scheduler(Thread):
    _count = 0
    _queue = Queue()
    _task_list = {}

    def run(self) -> None:
        """Запуск треда для выполнения задач."""
        while True:
            task = self._queue.get()
            task.status = TaskState.IN_PROGRESS
            task.run_task()

    @classmethod
    def put(cls, task: BaseTask) -> int:
        """
        Добавить задачу в очередь.

        :param BaseTask task: Задача
        :return Возвращает идентификатор задачи.
        """
        task.status = TaskState.IN_QUEUE
        task_id = cls._count
        cls._task_list.update({task_id: task})
        cls._queue.put(task)
        cls._count += 1
        return task_id

    def status(self, task_id: int) -> str:
        """
        Получить статус выполнения задачи.

        :param int task_id: Идентификатор задачи
        """
        task = self._task_list.get(task_id)
        if task:
            return task.status.value
        return 'Не найдено.'

    def result(self, task_id: int) -> str:
        """
        Получить результат работы задачи.

        :param int task_id: Идентификатор задачи
        """
        task = self._task_list.get(task_id)
        if task:
            return task.result
        return 'Не найдено.'
