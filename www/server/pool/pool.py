from threading import Thread

from www.server.task.base_task import BaseTask


class Pool(Thread):

    tasks = {}

    def __init__(self, task: BaseTask):
        self.put(task)

        self._task = task

