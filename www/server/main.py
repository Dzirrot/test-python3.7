import json
from io import BytesIO
from urllib import parse
from http.server import BaseHTTPRequestHandler, HTTPServer

from www.server.scheduler.scheduler import Scheduler
from www.server.task.task_manager import TaskManager


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    _scheduler = Scheduler()

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        params = json.loads(self.rfile.read(content_length).decode())
        if params:
            task = TaskManager.parse(params.get('method'), params.get('text'))
            task_id = self._scheduler.put(task)
            self.send_response(200)
            self.end_headers()
            response = BytesIO()
            response.write(f'{task_id}'.encode())
            self.wfile.write(response.getvalue())
        else:
            self.send_internal_error()

    def do_GET(self):
        params = dict(parse.parse_qsl(parse.urlparse(self.path).query))
        method = params.get('method')

        try:
            task_id = int(params.get('task_id'))
        except Exception:
            self.send_internal_error()
            return

        if method == 'status':
            result = self._scheduler.status(task_id)
        elif method == 'result':
            result = self._scheduler.result(task_id)

        self.send_response(200)

        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        response = BytesIO()
        response.write(f'{result}'.encode())

        self.wfile.write(response.getvalue())

    def send_internal_error(self):
        """Ошибка обработки запроса."""
        self.send_response(500)
        response = BytesIO()
        response.write('Internal Error.'.encode())
        self.wfile.write(response.getvalue())


if __name__ == '__main__':
    my_thread = Scheduler().start()
    httpd = HTTPServer(('localhost', 8000), SimpleHTTPRequestHandler)
    httpd.serve_forever()
