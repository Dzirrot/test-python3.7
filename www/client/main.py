import time
import json
import argparse
from urllib import request
from urllib.parse import urlencode

_URL = "http://localhost:8000"


class Workflow:

    def __init__(self, method='', text='', action=None, task_id=None):
        self.method = method
        self.text = text
        self.task_id = task_id
        self.action = action

    def do(self):
        if self.action is None:
            self.do_packet()
        else:
            self.do_simple()

    def do_simple(self):
        if self.action == 'add':
            result = self.add_task()
        else:
            result = self.get_info()
        return result

    def do_packet(self):
        self.task_id = self.add_task()
        self.action = 'status'

        attempt = 0
        while True:
            print(f'Попытка {attempt}')
            status = self.get_info()
            attempt +=1
            time.sleep(1)
            if status == 'Выполнено':
                self.action = 'result'
                break
        print(self.get_info())

    def add_task(self) -> str:
        """
        Добавить задачу.

        :return: Идентификатор задачи.
        """
        req = request.Request(_URL, json.dumps({'method': self.method, 'text': self.text}).encode())
        task = b''.join(request.urlopen(req).readlines()).decode()
        print(f'Задача добавлена. Идентификатор задачи: {task}. Тип: {self.method}\n')
        return task

    def get_info(self) -> str:
        """
        Получить состояние задачи.

        :return: Информацию по задаче.
        """
        url = _URL + '/?' + urlencode({'method': self.action, 'task_id': self.task_id})
        result = b''.join(request.urlopen(request.Request(url)).readlines()).decode()
        print(f'Запрос: {self.action}, результат: {result}')
        return result


class CheckParams:

    def __new__(cls, params: argparse.Namespace):
        assert params.Action in ['add', 'status', 'result', None], f'Не поддерживаемый тип запроса {params.Action}'

        if params.Action in ['add', None]:
            assert params.Method in ['reverse', 'swap'] \
                   and params.String is not None, (
                'При постановке задачи или пакетной обработке, необходимо указать метод (reverse|swap) '
                'и строку обработки.'
            )

        if params.Method in ['status', 'result']:
            assert params.Task is None, (
                'При запросе статуса или результата, необходимо указать идентификатор задачи.'
            )
        return params


def parse_arguments():
    parser = argparse.ArgumentParser(description='Add task and info about theirs states.')

    parser.add_argument('-a', '--Action', type=str,
                        help='If indicated, do an action, otherwise "packet" mode.')

    parser.add_argument('-t', '--Task', type=int,
                        help='If we need to get info about task, use with (status|result).')

    parser.add_argument('-m', '--Method', type=str,
                        help='What server need to do, with you string. Support: (reverse|swap).')

    parser.add_argument('-s', '--String', type=str, nargs='+',
                        help='Insert your string for processing.')

    return CheckParams(parser.parse_args())


if __name__ == '__main__':
    args = parse_arguments()
    workfl = Workflow(args.Method, ' '.join(args.String) if args.String else '', args.Action, args.Task)
    workfl.do()
