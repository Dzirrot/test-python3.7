from unittest import TestCase

from www.server.scheduler.scheduler import Scheduler
from www.server.task.base_task import TaskState
from www.server.task.task_manager import TaskManager


class SchedulerTest(TestCase):

    def test_1(self):
        """Добавление задачи в очередь."""
        task = TaskManager.parse('reverse', 'a1b2c3d4')
        task_id = Scheduler().put(task)

        self.assertEqual(TaskState.IN_QUEUE, Scheduler().status(task_id))
