# test-python3.7

Для старта сервера надо запустить **www/server/main.py **

Клиент **www/client/main.py**

Возможные параметры: --Action|-a, --Method|-m, --String|-s, --Task|-t.

**Используй** `python www/client/main.py -h`.


PS. Перестановка чётных букв с нечётными, не обрезает строки до чётного кол-ва
элементов.
